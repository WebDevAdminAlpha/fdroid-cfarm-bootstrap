---
- hosts: all
  become_method: su

  tasks:
    - name: "authorized_keys: set up the standard admins"
      authorized_key:
        user: root
        key: "{{ item.key }}"
        state: present
        unique: yes
      with_list: "{{ authorized_keys }}"

    - name: "apt: install debian packages for secure apt setup"
      apt:
        name: "{{item}}"
        state: latest
        install_recommends: no
        update_cache: yes
      with_items:
        - apt-transport-https
        - apt-transport-tor
        - debian-archive-keyring
        - gnupg
        - tor

    #- name: "apt_repository: sgvtcaew4bxjd7ln.onion as first repo"
    #  apt_repository:
    #    repo: 'deb tor+http://sgvtcaew4bxjd7ln.onion/debian-security stretch/updates main'
    #    filename: 0.sgvtcaew4bxjd7ln.onion
    #    update_cache: no

    #- name: "apt_repository: debian.osuosl.org stretch"
    #  apt_repository:
    #    repo: |
    #      deb https://debian.osuosl.org/debian/ stretch main
    #    update_cache: no

    #- name: "apt_repository: debian.osuosl.org stretch-updates"
    #  apt_repository:
    #    repo: |
    #      deb https://debian.osuosl.org/debian/ stretch-updates main
    #    update_cache: no

    #- name: "apt_repository: debian.osuosl.org stretch-backports"
    #  apt_repository:
    #    repo: |
    #      deb https://debian.osuosl.org/debian/ stretch-backports main
    #    update_cache: no

    #- name: "apt_repository: deb.debian.org debian-security"
    #  apt_repository:
    #    repo: |
    #      deb https://deb.debian.org/debian-security/ stretch/updates main
    #    update_cache: no

    #- name: "apt_repository: security.debian.org"
    #  apt_repository:
    #    repo: 'deb http://security.debian.org/debian-security stretch/updates main'
    #    update_cache: no

    #- name: "copy: clear /etc/apt/sources.list"
    #  copy:
    #    content: ""
    #    dest: "/etc/apt/sources.list"


    - name: configure apt repos for stretch (debian 9)
      block:
        - name: "apt_repository: debian.osuosl.org stretch-backports"
          apt_repository:
            repo: |
              deb https://debian.osuosl.org/debian/ stretch-backports main
            update_cache: no
        - name: "copy: apt pinning rule for backports packages"
          copy:
            content: |
              Package: vagrant ruby-net-ssh
              Pin: release a=stretch-backports
              Pin-Priority: 500
            dest: /etc/apt/preferences.d/debian-stretch-backports.pref
          become: yes
      when: ansible_distribution_release == 'stretch'

    - name: configure apt repos for buster (debian 10)
      block:
        - name: "apt_repository: debian.osuosl.org buster-backports"
          apt_repository:
            repo: |
              deb https://debian.osuosl.org/debian/ buster-backports main
      when: ansible_distribution_release == 'buster'

    - name: "apt: dist-upgrade"
      apt:
        update_cache: yes
        upgrade: dist

    - name: "apt: install debian packages"
      apt:
        name: "{{item}}"
        state: latest
        autoclean: yes
        autoremove: yes
        install_recommends: no
      with_items:

        # essential utilities
        - bash-completion
        - curl
        - elpa-markdown-mode
        - emacs-nox
        - emacs-goodies-el
        - figlet
        - git
        - htop
        - iotop
        - less
        - ncdu
        - nethogs
        - lvm2
        - screen
        - unattended-upgrades
        - vim
        - wget
        - yaml-mode

        # libvirt/KVM minimal
        - libvirt-clients
        - libvirt-daemon
        - libvirt-daemon-system
        - nfs-kernel-server
        - busybox
        - libguestfs-tools
        - bridge-utils
        - dnsmasq-base
        - ebtables
        - netcat-openbsd
        - qemu-kvm

        - vagrant
        - vagrant-mutate
        - vagrant-libvirt

        - python-libvirt
        - python-lxml

      become: yes

    - name: "file: losen permissions on /root for enabling vagrant nfs v4 support"
      file:
        path: /root
        mode: 0701

    - name: "file: create symbolic link to enable all locales"
      file:
        src: "/usr/share/i18n/SUPPORTED"
        dest: "/etc/locale.gen"
        state: link
        force: yes
    - name: "locale_gen: generate all locales"
      locale_gen: "name={{item}} state=present"
      with_lines:
        - "grep -Eo '^ *[^#][^ ]+' /etc/locale.gen"
      
    - name: "timezone: set system to Etc/UTC"
      timezone:
        name: Etc/UTC
      become: yes
    - name: 'lineinfile: set default system locale to en_US.UTF-8'
      lineinfile:
        dest: "/etc/default/locale"
        line: "LANG=en_US.UTF-8"
      become: yes

    - name: "include_role: ensure kvm nesting is enabled"
      include_role: name=ansible-debian-enable-kvm-nesting
      become: yes

    - lvol:
        vg: lvm
        lv: fdroidMirror
        size: 300G
    - filesystem:
        dev: /dev/lvm/fdroidMirror
        fstype: ext4
    - mount:
        path: /srv/mirror.f-droid.org
        src: /dev/lvm/fdroidMirror
        fstype: ext4
        state: mounted

    - lvol:
        vg: lvm
        lv: libvirtImagesPart
        size: 2T
    - filesystem:
        dev: /dev/lvm/libvirtImagesPart
        fstype: ext4
    - mount:
        path: /var/lib/libvirt/images
        src: /dev/lvm/libvirtImagesPart
        fstype: ext4
        state: mounted

    - name: "virt_pool: setup 'default' libvirt storage pool"
      virt_pool:
        name: default
        state: present
        xml: |
          <pool type="dir">
            <name>default</name>
            <source>
            </source>
            <target>
              <path>/var/lib/libvirt/images</path>
              <permissions>
                <mode>0711</mode>
                <owner>0</owner>
                <group>0</group>
              </permissions>
            </target>
          </pool>
    - name: "virt_pool: start 'default' libvirt storage pool"
      virt_pool:
        name: default
        state: active
    - name: "virt_pool: autostart 'default' libvirt storage pool"
      virt_pool:
        name: default
        autostart: yes

    - name: "shell: set motd"
      shell: |
        echo > /etc/motd
        figlet `hostname | cut -d . -f 1` >> /etc/motd
        printf '\ncreated with https://gitlab.com/fdroid/fdroid-cfarm-bootstrap\n\n' >> /etc/motd

    - name: "copy: script for updating with apt"
      copy:
        mode: 0700
        content: |
          #!/bin/sh

          set -x
          apt-get update
          apt-get -y dist-upgrade --download-only

          set -e
          apt-get -y upgrade
          apt-get dist-upgrade
          apt-get autoremove --purge
          apt-get clean
        dest: /root/update-all

    - name: "user: create user for tunneling to VMs"
      user:
        name: tunnel
        shell: /bin/false
    - name: "authorized_keys: set admins up the ssh vm tunnel access"
      authorized_key:
        user: tunnel
        key: "{{ item.key }}"
        state: present
        unique: yes
      with_list: "{{ authorized_keys }}"
    - name: "authorized_keys: set up tunnel users"
      authorized_key:
        user: tunnel
        key: "{{ item.key }}"
        state: present
        unique: yes
      with_list: "{{ tunnel_authorized_keys }}"

    - name: "copy: cron job for updating vagrant global-status cache"
      copy:
        mode: 0755
        owner: root
        group: root
        content: |
          #!/bin/sh
          for f in `vagrant global-status 2> /dev/null | grep -Eo '^[0-9a-f]{7}'`; do
              vagrant status $f > /dev/null 2>&1
          done
        dest: /etc/cron.hourly/update-vagrant-global-status-cache
